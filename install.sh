#!/bin/bash
cargo build --release
sudo mkdir -p /usr/local/bin/
sudo cp ./target/release/legion_brightness_sync /usr/local/bin/
sudo cp ./supported_devices.toml /usr/local/bin/

sudo sh -c "cat >/etc/systemd/system/legion_brightness_sync.service" <<-EOF
[Unit]
Description=Syncs mux brightness
After=network.target
StartLimitIntervalSec=0

[Service]	
Type=simple
Restart=always
RestartSec=1
WorkingDirectory=/usr/local/bin
ExecStart=/usr/local/bin/legion_brightness_sync

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable --now legion_brightness_sync.service