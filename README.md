# Lenovo Mux Brightness Sync 

This is file contains instructions that are designed to fix the backlight sync issues on the 2021 Lenovo Slim 7 AMD. 
This fix may work on other devices, it may not.
If you wish to contribute or contact me about supporting your device, either submit a PR or message me on discord @ Covalent#5766.

## Install Instructions
0. cargo must be installed -> https://doc.rust-lang.org/cargo/getting-started/installation.html
1. In terminal `git clone https://gitlab.com/lukehmcc/mux_brightness_sync.git`
2. `cd mux_brightness_sync`
3. `./install.sh`

### Caveats 
- This script requires you to be using `systemd`. If you are not using systemd you can probably figure out how to
install this anyway though.
- This is a hacky solution to a kernel level probablem. Hopefully I can figure out the brightness issues at some point
and address them properly. But for now this is what we have.
- You may notice that while under "hybrid" graphics, the brightness seems to jump around. This is a power management 
thing and is not related to this script. 
- If you use ACPI kernel modifiers it may break this script.