// this sync brightness between muxed display outputs
// spefic laptops have to be supported

use futures::{
    channel::mpsc::{channel, Receiver},
    SinkExt, StreamExt,
};
use notify::{Event, RecommendedWatcher, RecursiveMode, Watcher, Config};
use std::{path::Path, io::{Read, Write}};
use std::fs;
use std::string::String;
use serde::{self, Deserialize, Serialize};
extern crate std;

// supported devices struct 
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(crate = "self::serde")] // must be below the derive attribute
struct SupportedDevices {
    read_devices: Vec<String>,
    write_devices: Vec<String>,
    nvidia_to_amd: bool,
}

// Reads /sys/class/backlight and checks if there are compatible read and write files available
// if there are, sync them in the direction specificed by config
fn main() {
    let devices_toml_string = fs::read_to_string("./supported_devices.toml")
    .expect("Should have been able to read \"supported devices\"");
    let supported_devices: SupportedDevices = toml::from_str(&devices_toml_string).unwrap();
    let class_paths_dir = fs::read_dir("/sys/class/backlight").expect("please run with sudo privladges");
    let mut class_paths: Vec<String> = Vec::new();
    for class_path in class_paths_dir {
        // This line basically does /etc/lol -> lol and appends it to class paths
        class_paths.push(class_path.unwrap().path().to_str().unwrap().rsplit_terminator('/').next().unwrap().to_owned());
    }
    let mut readd: Option<String> = None;
    let mut writed: Option<String> = None;
    for read_device in supported_devices.read_devices {
        if class_paths.contains(&read_device){
            readd = Some(read_device);
        }
    }
    for write_device in supported_devices.write_devices {
        if class_paths.contains(&write_device){
            writed = Some(write_device);
        }
    }
    let read_path_string: String = format!("{}{}{}", "/sys/class/backlight/",readd.unwrap(),"/brightness");
    let read_path: &Path = Path::new(&read_path_string);
    let write_path_string: String = format!("{}{}{}", "/sys/class/backlight/",writed.unwrap(),"/brightness");
    let write_path: &Path = Path::new(&write_path_string);
    println!("watching {}", read_path.display());
    println!("writing to {}", write_path.display());

    futures::executor::block_on(async {
        if let Err(e) = async_watch(&read_path, &write_path).await {
            println!("error: {:?}", e)
        }
    });
}

fn async_watcher() -> notify::Result<(RecommendedWatcher, Receiver<notify::Result<Event>>)> {
    let (mut tx, rx) = channel(1);
    let watcher = RecommendedWatcher::new(move |res| {
        futures::executor::block_on(async {
            tx.send(res).await.unwrap();
        })
    }, Config::default())?;

    Ok((watcher, rx))
}

async fn async_watch<P: AsRef<Path>>(read_path: P, write_path: P) -> notify::Result<()> {
    let (mut watcher, mut rx) = async_watcher()?;

    // Add a path to be watched. All files and directories at that path and
    // below will be monitored for changes.
    watcher.watch(read_path.as_ref(), RecursiveMode::Recursive)?;

    while let Some(res) = rx.next().await {
        match res {
            Ok(_) => match read_then_write(&read_path, &write_path){
                Ok(_) => (),
                Err(e) => println!("Copy error: {}", e)
            },
            Err(e) => println!("watch error: {:?}", e),
        }
    }

    Ok(())
}

fn read_then_write<P: AsRef<Path>>(read_path: P, write_path: P) -> std::io::Result<()>{
    let mut file_read = fs::File::open(read_path)?;
    let mut buf = String::new();
    file_read.read_to_string(&mut buf)?;
    let value: u64 = str::parse(&buf.trim()).unwrap();
    let valuef: f64 = value as f64;
    let value_mult: u64 = (valuef * 2.55) as u64; 
    let mut file_write = fs::File::create(write_path)?;
    file_write.write_all(&value_mult.to_string().as_bytes())?;
    println!("Read value is: {}\nWrite value is: {}",value,value_mult);
    Ok(())
}